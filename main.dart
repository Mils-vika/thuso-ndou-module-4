import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:splash/Home.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Shed',
        theme: ThemeData(
          primarySwatch: colors.purple,
        ),
        home: AnimatedSplashScreen(
            duration: 3000,
            splash: Image.asset("assets/shed.png"),
            nextScreen: Home(),
            splashTransition: SplashTransition.fadeTransition,
            backgroundColor: Colors.white));
  }
}
